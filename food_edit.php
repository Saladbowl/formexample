<style>

body{
    background-color: #ece9d8;
    text-align:center;
}

table.center {
    margin-left:auto; 
    margin-right:auto;
}

</style>

<?php session_start();
require_once('inc/food.class.php');
require_once('inc/login.class.php');

$user = new login();
$user-> userValidTest();

$food = new food();

if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0)
{
    $food->load($_REQUEST['id']);
}

if (isset($_POST['id'])){

    $food->setData($_POST);
    
    $errors = $food->validate();
    
    if (count($errors) == 0){
        $food->save();
        header("Location: food.php");
        exit;
    }
    
}

?>

<html>
    <body>
        
        <?php 
           if(!empty($errors)){
             var_dump($errors); 
           }
         ?>
            

        <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="POST">
            Place:<br> <input type="text" name="Name" required="required" value="<?php echo htmlspecialchars_decode($food->foodName, ENT_HTML5); ?>"/><br> <!-- Turns the sanitized stuff back into HTML -->
            Website:<br> <input type="text" name="Website" value="<?php echo htmlspecialchars_decode($food->foodWeb, ENT_HTML5); ?>"/><br> <!-- Turns the sanitized stuff back into HTML -->
            Type:
<br><select name="Type">
<option value="Asian" <?php if ($food->foodType=="Asian") echo 'selected="selected"';?>>Asian</options>
<option value="Greek" <?php if ($food->foodType=="Greek") echo 'selected="selected"';?>>Greek</options>
<option value="Mexican" <?php if ($food->foodType=="Mexican") echo 'selected="selected"';?>>Mexican</options>
<option value="Indian" <?php if ($food->foodType=="Indian") echo 'selected="selected"';?>>Indian</options>
<option value="Burgers" <?php if ($food->foodType=="Burgers") echo 'selected="selected"';?>>Burgers</options>
<option value="Fast Food" <?php if ($food->foodType=="Fast Food") echo 'selected="selected"';?>>Fast Food</options>
<option value="Deli" <?php if ($food->foodType=="Deli") echo 'selected="selected"';?>>Deli</options>
<option value="Steak n' Salad" <?php if ($food->foodType=="Steak n' Salad") echo 'selected="selected"';?>>Steak n' Salad</options>
<option value="Pizza" <?php if ($food->foodType=="Pizza") echo 'selected="selected"';?>>Pizza</options>
<option value="Other" <?php if ($food->foodType=="Other") echo 'selected="selected"';?>>Other</options>
</select><br>
            Ian:
<br><select name="Ian">
<option value="Yes" <?php if ($food->foodIan=="Yes") echo 'selected="selected"';?>>Yes</options>
<option value="No" <?php if ($food->foodIan=="No") echo 'selected="selected"';?>>No</options>
<option value="Maybe" <?php if ($food->foodIan=="Maybe") echo 'selected="selected"';?>>Maybe</options>
</select><br>
            Elaine:
<br><select name="Elaine">
<option value="Yes" <?php if ($food->foodElaine=="Yes") echo 'selected="selected"';?>>Yes</options>
<option value="No" <?php if ($food->foodElaine=="No") echo 'selected="selected"';?>>No</options>
<option value="Maybe" <?php if ($food->foodElaine=="Maybe") echo 'selected="selected"';?>>Maybe</options>
</select><br>
            Price:
<br><select name="Price">
<option value="Cheap" <?php if ($food->foodPrice=="Cheap") echo 'selected="selected"';?>>Cheap</options>
<option value="Med" <?php if ($food->foodPrice=="Med") echo 'selected="selected"';?>>Medium</options>
<option value="High" <?php if ($food->foodPrice=="High") echo 'selected="selected"';?>>High</options>
</select><br>
            Rating:
<br><select name="Rate">
<option value="No Rating" <?php if ($food->foodRate=="No Rating") echo 'selected="selected"';?>>No Rating</options>
<option value="1/5" <?php if ($food->foodRate=="1/5") echo 'selected="selected"';?>>1/5</options>
<option value="2/5" <?php if ($food->foodRate=="2/5") echo 'selected="selected"';?>>2/5</options>
<option value="3/5" <?php if ($food->foodRate=="3/5") echo 'selected="selected"';?>>3/5</options>
<option value="4/5" <?php if ($food->foodRate=="4/5") echo 'selected="selected"';?>>4/5</options>
<option value="5/5" <?php if ($food->foodRate=="5/5") echo 'selected="selected"';?>>5/5</options>
</select><br>
            <input type="hidden" name="id" value="<?php echo $food->foodId; ?>"/><br>
            <input type="submit" name="submit">
        </form>

<p><a href='food.php'>Return to Food Spots</a></p>   
    
    </body>
</html>