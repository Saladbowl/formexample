<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Ian Frank - Form Example</title>

<style>
body{
    background-color: #ece9d8;
    text-align:center;
}
table.center {
    margin-left:auto; 
    margin-right:auto;
}
</style>

<?php

require_once('inc/sanitize.inc.php');
require_once('inc/food.class.php');
$food = new food();

$result = $food->getFoodSpots();
?>

</head>

<body>

<h1>Food Spots Form Example</h1>

<h2><a href='../'>Return to Portfolio</a></h2>

<h2><a href='login.php'>Login to make changes</a></h2>

<div>

	<a href='food_edit.php'>Add new spots</a>

	<table border="1" class="center">
	<tr>
		<th>Place</th>
		<th>Food Type</th>
		<th>Ian</th>
		<th>Mom</th>
		<th>Price Range</th>
		<th>Rating</th>
	</tr>    
<?php
	while($row = mysqli_fetch_array($result)){		//Turn each row of the result into an associative array 
  	
		//For each row you found int the table create an HTML table in the response object
  		echo "<tr>";
                if(empty($row['Website'])){
  		   echo "<td>" . $row['Name'] . "</td>";
                } else {
                   echo "<td><a href=" . $row['Website'] . ">". $row['Name'] . "</a></td>";
                }
  		echo "<td>" . $row['Type'] . "</td>";
  		echo "<td>" . $row['Ian'] . "</td>";
  		echo "<td>" . $row['Elaine'] . "</td>";
  		echo "<td>" . $row['Price'] . "</td>";
  		echo "<td>" . $row['Rate'] . "</td>";
		  echo "<td><a href='food_edit.php?id=" . $row['id'] . "'>Update</a></td>";
      echo "<td><a href='deleteForm.php?id=" . $row['id'] ."' onclick='return confirm()'>Delete</a></td>";
  		echo "</tr>";
  	}

	//echo "</table>";		//Placed this command in the HTML instead of using the echo
?>
	</table>

</div>


</body>
</html>