<?php  session_start();

if($_SESSION['validUser']=="yes")			//For a valid user that has set up a session
{
	$_SESSION['validUser'] = "";
        $_SESSION['admin'] = "";			//Set session variable to empty
	unset($_SESSION['validUser']);
        unset($_SESSION['level']);			//Unset the session variable
	session_destroy();						//destroy the session attached to this page
	header("Location:login.php");	//Redirect the user to the sign in page
	exit;									//If still here for some reason close the PHP session
}
else {
	header("Location:login.php");	//Invalid user then send them to the login page
}

?>