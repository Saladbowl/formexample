<?php
require_once('inc/sanitize.inc.php'); //Used on any lines that are inputs not dropdown

class food{
    /* Variable Storage */
    var $foodId;
    var $foodName;
    var $foodType;
    var $foodIan;
    var $foodElaine;
    var $foodPrice;
    var $foodWeb;
    var $foodRate;

    /* Set Variables */
    function setData($dataArray){

        $this->foodId = $dataArray['id'];
        $this->foodName = sanitize_html_string($dataArray['Name']); //Turn characters into HTML char code
        $this->foodType = $dataArray['Type'];
        $this->foodIan = $dataArray['Ian'];
        $this->foodElaine = $dataArray['Elaine'];
        $this->foodPrice = $dataArray['Price'];
        $this->foodWeb = sanitize_html_string($dataArray['Website']); //Turn characters into HTML char code
        $this->foodRate = $dataArray['Rate'];
    }

    
    /* Get information from DB based on ID to be used for edits*/
    function load($Id){
        $success = false;
        
require_once('inc/connect2.php'); //DB connect puts DB connection in $db

        if ($db){
        
            
            $loadfoodSQL = "SELECT * FROM FormExample WHERE id = " .  
                mysqli_real_escape_string($db, $Id);            
            //var_dump($loadfoodSQL);die;          //Testing
            $rs = mysqli_query($db, $loadfoodSQL);
            
            if ($rs){                
                $foodData = mysqli_fetch_assoc($rs);
//                var_dump($foodData);die;
                $this->setData($foodData);
                $success = ($this->foodId > 0 ? true : false); //Checks to make sure if it got anything back, if so returns true
            }else{
                echo mysqli_error($db);
                die;                
            }
        }else{
            echo mysqli_error($db);
            die;
        }
        
        return $success;
    }
    /* Save Info to DB from updates or inserts*/
    function save(){
        require_once('inc/connect2.php'); //DB connect puts DB connection in $db
        if ($db){
        
            if ($this->foodId > 0) { //Check to see if there is an ID
                // this is an update
                $foodUpdateSQL = "UPDATE FormExample SET " .
                    "Name = '" . mysqli_real_escape_string($db, $this->foodName) . "', " .
                    "Type = '" . mysqli_real_escape_string($db, $this->foodType) . "', " .
                    "Ian = '" . mysqli_real_escape_string($db, $this->foodIan) . "', " .
                    "Elaine = '" . mysqli_real_escape_string($db, $this->foodElaine) . "', " .
                    "Price = '" . mysqli_real_escape_string($db, $this->foodPrice) . "', " .
                    "Website = '" . mysqli_real_escape_string($db, $this->foodWeb) . "', " .
                    "Rate = '" . mysqli_real_escape_string($db, $this->foodRate) . "' " .
                    "WHERE id = " . mysqli_real_escape_string($db, $this->foodId);

                $rs = mysqli_query($db, $foodUpdateSQL);     
                if (!$rs) {
                    echo mysqli_error($db);
echo "<br>";
 var_dump($foodUpdateSQL);
                    die;
                }

            }else{
                // this is an insert
                $foodInsertSQL = "INSERT INTO FormExample SET " . 
                    "Name = '" . mysqli_real_escape_string($db, $this->foodName) . "', " .
                    "Type = '" . mysqli_real_escape_string($db, $this->foodType) . "', " .
                    "Ian = '" . mysqli_real_escape_string($db, $this->foodIan) . "', " .
                    "Elaine = '" . mysqli_real_escape_string($db, $this->foodElaine) . "', " .
                    "Price = '" . mysqli_real_escape_string($db, $this->foodPrice) . "', " .
                    "Website = '" . mysqli_real_escape_string($db, $this->foodWeb) . "', " .
                    "Rate = '" . mysqli_real_escape_string($db, $this->foodRate) . "' ";

                $rs = mysqli_query($db, $foodInsertSQL);     
                if (!$rs) {
                    echo mysqli_error($db);
echo "<br>";
 var_dump($foodInsertSQL);
                    die;
                }else{
                    $this->foodId = mysqli_insert_id($db);
                }

            }
        }
    }


function validate(){
        $errorsArray = array();
        
        if (empty($this->foodName)){
            $errorsArray['foodName'] = "Location is required.";
        }
      
        return $errorsArray;
    }
/*  */
function getFoodSpots($type = null, $price = null){ //Input incase a a sort would be used
        $rs = null;
        
        require_once('inc/connect2.php'); //DB connect puts DB connection in $db
        
        if ($db){
            
            $getFoodSQL = "SELECT * FROM FormExample";

            $whereClause = "";
            
            if (!is_null($type) && !empty($type)){
                $whereClause .= (!empty($whereClause) ? "AND" : "" ) . " Type = '" . mysqli_real_escape_string($db, $type) . "'";
            }

            if (!is_null($price) && !empty($price)){
                $whereClause .= (!empty($whereClause) ? "AND" : "" ) . " Price = '" . mysqli_real_escape_string($db, $price) . "'";
            }

            
            $getFoodSQL .= (!empty($whereClause) ? " WHERE " . $whereClause : "" );

            
            $rs = mysqli_query($db, $getFoodSQL);     
            if (!$rs){
                echo mysqli_error($db);
                die;
            }            
        }
        return $rs;
}    



}